<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class ScoreController extends Controller
{
    /**
     * ratios of exercises
     */
    private $ratios = [
        'e1' => 2,
        'e2' => 1,
        'e3' => 3,
        'e4' => 2,
        'e5' => 5,
        'e6' => 2,
        'e7' => 1,
        'e8' => 12,
    ];

    /**
     *Scores file path
     */
//    private string $scoreFilePath = \Storage::path('scores/scores.xlsx');
    private $scoreFilePath = 'scores/scores.xlsx';

    /**
     * final scores txt file path
     */
    private $finalScoresFilePath = 'scores/finalScores.txt';

    /**
     *reads the file of scores and prepare a text file including scores for download.
     * Note: text file will be deleted after download
     */
    public function downloadScores()
    {
        /**
         * read the scores file
         */
        $allStudentsScores = (new FastExcel)->import(\Storage::path($this->scoreFilePath));

        /**
         * calculate the final score of each student and write it in a text file
         */
        foreach ($allStudentsScores as $studentScores) {
            $name = $studentScores[''];
            unset($studentScores['']);

            $finalScore = $this->calculateScore($studentScores);

            /**
             * record final score
             */
            $this->prepareFinalScoresFile($name, $finalScore);
        }

        /**
         * make a download response and delete file after download is complete
         */
        return response()->download(\Storage::path($this->finalScoresFilePath))->deleteFileAfterSend(true);
    }

    /**
     * calculates the final score of student
     * @param array $student an associative array of scores of a student
     * @return int final score of the student
     */
    private function calculateScore(array $student)
    {
        /**
         * calculate wighted summation of scores
         */
        $totalScore = 0;
        foreach ($student as $exercise => $score) {
            $totalScore += $score * $this->ratios[$exercise];
        }

        /**
         * calculate some of ratios
         */
        $ratiosSum = 0;
        foreach ($this->ratios as $ratio) {
            $ratiosSum += $ratio;
        }

        /**
         * determine the weighted average of the student score
         */
        return (int)ceil($totalScore / $ratiosSum);
    }

    /**
     * write the student score in a txt file
     * @param $name string name of the student
     * @param int $finalScore score of the student
     */
    private function prepareFinalScoresFile(string $name, int $finalScore)
    {
        $content = $name . '=' . $finalScore;
        \Storage::append($this->finalScoresFilePath, $content);
    }
}
